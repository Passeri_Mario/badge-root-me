<?php
    session_start();

    if (isset($_SESSION['token']) AND isset($_POST['token']) AND !empty($_SESSION['token']) AND !empty($_POST['token'])){

        $name = htmlentities($_POST['name']);

        if (($_SESSION['token'] === $_POST['token']) && isset($_POST['name']) and !empty($_POST['name'])) {

            include "config/api.php" ;

            require 'class/badge.php';

            if($data !== null){

                $badge_utilisateur = new Badges($data['id_auteur'], $data['nom'], $data["score"], $data['rang'], $data['position'] );

                $id_user = $badge_utilisateur->getId();

                $name_user = $badge_utilisateur->getNom();

                $score_user = $badge_utilisateur->getScore();

                $rang_user = $badge_utilisateur->getrang();

                $position_user = $badge_utilisateur->getPosition();

                include ("view/generator.phtml");

            }
            else{
                session_destroy();
                header('Location: /index');
            }
        }
        else{
            session_destroy();
            header('Location: /index');
        }
    }
    else{
        session_destroy();
        header('Location: /index');
    }


