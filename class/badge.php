<?php

class Badges
{
    private $id;
    private $nom; // nom
    private $score; // score
    private $rang; // rang
    private $position;

    /**
     * @param $id
     * @param $nom
     * @param $score
     * @param $rang
     * @param $position
     */

    public function __construct($id, $nom, $score, $rang, $position)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->score = $score;
        $this->rang = $rang;
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param mixed $score
     */
    public function setScore($score): void
    {
        $this->score = $score;
    }

    /**
     * @return mixed
     */
    public function getRang()
    {
        return $this->rang;
    }

    /**
     * @param mixed $rang
     */
    public function setRang($rang): void
    {
        $this->rang = $rang;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position): void
    {
        $this->position = $position;
    }

}





