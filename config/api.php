<?php

    htmlspecialchars($UID = $_POST['name']);
    htmlspecialchars($api_key = $_POST['apikey']);

    $query = array(
        "http"=>array("method"=>"GET", "header"=>"Cookie: api_key=".$api_key."\r\n")
    );

    $api = stream_context_create($query);

    $donnes = file_get_contents("https://api.www.root-me.org/auteurs/".$UID, true, $api);

    try {
        $data = json_decode($donnes, true, 512, JSON_THROW_ON_ERROR);
    }
    catch (JsonException $e) {
    }

    // DEV TEST
    /*echo '<pre>';
    print_r($data);
    echo '</pre>';*/