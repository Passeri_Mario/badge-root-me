<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Voici les routes pour enregistrer des itinéraires Web de l'application.
| Les routes sont chargées par $_SERVER['REQUEST_URI']
|
*/

$request = $_SERVER['REQUEST_URI'];

switch ($request) {

    // 404
    default:
        http_response_code(404);
        require __DIR__ . '/controller/404.php';
        break;

    // home
    case '/' :
    case '/index':
        http_response_code(200);
        require __DIR__ . '/controller/index.php';
        break;

    case'/generator':
        http_response_code(200);
        require __DIR__ . '/controller/generator.php';
        break;


}
