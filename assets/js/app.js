let text = document.querySelector('.typing-text');
let form = document.getElementById('form');

window.addEventListener('keydown', function(event) {
    if (event.code === "Enter") {
        text.style.display = "block";
        form.style.display="block";
    }
});

let card = document.getElementById('card');
card.addEventListener('click', function() {
    text.style.display = "block";
    form.style.display="block";
});

